package generali.testView;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import application.V.GUI_OrganizzazioneUtentiLogicsImpl;

class userTest {
	
	GUI_OrganizzazioneUtentiLogicsImpl r = new GUI_OrganizzazioneUtentiLogicsImpl();  
	
    @Test
     void testNewUser() {   
        String tx = "user";
        r.leggiElencoConMAPPA();
        r.scriviNuovoUtenteConMAPPA(tx, null);       
        assertEquals(r.getMappa().get(tx).getUsername(),tx);       
    }
   
    @Test
    void testUserAlreadyPresent() {     
        String tx = "user";     
        r.scriviNuovoUtenteConMAPPA(tx, null);     
        assertEquals(true, r.UtenteGiaPresente(tx));
       
    }
   
    @Test
    void testUserNotPresent() {
        String tx = "user";    
        r.scriviNuovoUtenteConMAPPA(tx, null);       
        assertEquals(false, r.UtenteGiaPresente("user2"));
       
    }
}
