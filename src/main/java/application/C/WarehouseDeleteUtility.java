package application.C;

import application.M.Catena;

/**
 * @author marco
 *
 */
public interface WarehouseDeleteUtility {

	/**
	 * Delete an information from Tipologia
	 * 
	 * @param catena the container of typology
	 * @param the    container of typology
	 * @param idInfo the info to be deleted
	 */
	void deleteTipo(final Catena catena, final String idTipo, final String key);

	/**
	 * Delete an information or a food waste from Prodotto
	 * 
	 * @param catena   the container of typology
	 * @param idProd   the name of the product
	 * @param key      the name of the info to be deleted
	 * @param idScarto the name of the waste to be deleted
	 */
	void deleteProdotto(final Catena catena, final String idProd, final String key, final String idScarto);
	
	/**
	 * Delete an information or a food waste from ProdConcreto
	 * 
	 * @param catena    the container of typology
	 * @param idProdCon the name of the product
	 * @param key       the name of the info to be deleted
	 * @param idScarto  the name of the waste to be deleted
	 */
	void deleteProdCon(final Catena catena, final String idProdCon, final String key, final String idScarto);

	/**
	 * Delete an information or a food waste from ProdFornito
	 * 
	 * @param catena    the container of typology
	 * @param idProdFor the name of the product
	 * @param key       the name of the info to be deleted
	 * @param idScarto  the name of the waste to be deleted
	 */
	void deleteProdFor(final Catena catena, final String idProdFor, final String key, final String idScarto);

	/**
	 * Delete a Hotel
	 * 
	 * @param catena  the container of typology
	 * @param idHotel the name of the hotel to be deleted
	 */
	void deleteHotel(final Catena catena, final String idHotel);

	/**
	 * Delete a food storage from the Hotel
	 * 
	 * @param catena     the container of typology
	 * @param idDispensa the of the dispensa to be deleted
	 * @param idHotel    the name of the hotel
	 */
	void deleteDispensa(final Catena catena, final String idDispensa, final String idHotel);


	/**
	 * Delete a product from the Fornitore
	 * 
	 * @param catena      the container of typology
	 * @param idFornitore the name of the supplier
	 * @param idProdotto  the name of the product to be deleted
	 */
	void deleteFornitore(final Catena catena, final String idFornitore, final String idProdotto);
}
