package application.C;

public class DateNotFound extends Exception{
    /**
     * 
     */
    private static final long serialVersionUID = -4107497437767511929L;

    public DateNotFound(String msg){ 
        super(msg);   
     } 

}
