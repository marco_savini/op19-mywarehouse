	package application.C;

import java.util.ArrayList;
import java.util.HashMap;

import application.M.*;

/**
 * @author marco
 *
 */

public class WarehouseAddUtilityImpl implements WarehouseAddUtility {

	/*
	 * The array that saves the Object to be added
	 */
	final private ArrayList<Typology> typology = new ArrayList<>();

	/*
	 * The array that saves the food waste to be added
	 */
	final private ArrayList<Scarto> scarti = new ArrayList<>();

	@Override
	public Tipologia addNewTipologia(final Catena catena, final String id, final String key, final String value) {
		Tipologia tipologia = null;
		final HashMap<String, String> info = new HashMap<>();

		if (!catena.ottieniDallInventario(id).isPresent() && !id.isBlank()) {
			System.out.println("La tipologia non esiste e viene aggiunta");
			tipologia = new Tipologia(id);
			this.typology.add(tipologia);
			catena.aggiungiAllInventario(this.typology);
			if (!key.isBlank()) {
				info.put(key, value);
				tipologia.aggiungiInfo(info);
				
			}
			this.typology.add(tipologia);
			catena.aggiungiAllInventario(this.typology);
		}
		return tipologia;
	}

	@Override
	public Prodotto addNewProdotto(final Catena catena, final String idProdotto, final String idTipo,
			final String idScarto, final String valoreScarto, final String key, final String value) {
		
		Prodotto prodotto = null;
		Scarto scarto = null;
		Tipologia tipologia = null;
		final HashMap<String, String> info = new HashMap<>();
				
		if (!catena.ottieniDallInventario(idProdotto).isPresent() && !idProdotto.isBlank()) {
			tipologia = (Tipologia) catena.ottieniDallInventario(idTipo).get();
			System.out.println("Il prodotto non esiste e viene creato");
			prodotto = new Prodotto(idProdotto, tipologia);

			if (!key.isBlank()) {
				info.put(key, value);
				prodotto.aggiungiInfo(info);
			}
			if (!idScarto.isBlank() && !valoreScarto.isBlank()) {
				scarto = new Scarto(idScarto);				
				scarto.setQuantita(Float.parseFloat(valoreScarto));
				this.scarti.add(scarto);
				prodotto.aggiungiScarti(this.scarti);
				catena.aggiungiScarti(this.scarti);
			}
			this.typology.add(prodotto);
			catena.aggiungiAllInventario(this.typology);
		}
		return prodotto;
	}

	@Override
	public ProdConcreto addNewProdConcreto(final Catena catena, final String idProdCon, final String idProdotto,
			final String idScarto, final String valoreScarto, final String key, final String value) {

		ProdConcreto prodotto = null;
		Scarto scarto = null;
		Prodotto prodPadre = null;
		final HashMap<String, String> info = new HashMap<>();

		if (!catena.ottieniDallInventario(idProdCon).isPresent() && !idProdCon.isBlank() && 
				catena.ottieniDallInventario(idProdotto).isPresent()) {
			prodPadre = (Prodotto) catena.ottieniDallInventario(idProdotto).get();
			System.out.println("Il prodConcreto non esiste e viene creato");

			prodotto = new ProdConcreto(idProdCon, prodPadre);
			prodotto.setIDMigliorFornitore(prodotto.getIDMigliorFornitore(catena));
			prodotto.setPrezzoPiuBasso(prodotto.getPrezzoPiuBasso(catena));
			prodotto.setPrezzoEffettivoMigliore(prodotto.getPrezzoEffettivoMigliore(catena));

			if (!key.isBlank()) {
				info.put(key, value);
				prodotto.aggiungiInfo(info);
			}
			if (!idScarto.isBlank() && !valoreScarto.isBlank()) {
				scarto = new Scarto(idScarto);
				scarto.setQuantita(Float.valueOf(valoreScarto));
				this.scarti.add(scarto);
				prodotto.aggiungiScarti(this.scarti);
				catena.aggiungiScarti(this.scarti);
			}
			this.typology.add(prodotto);
			catena.aggiungiAllInventario(this.typology);
		}
		return prodotto;
	}

	@Override
	public ProdFornito addNewProdFornito(final Catena catena, final String idProdFor, final String idProdCon,
			final String idScarto, final String valoreScarto, final String key, final String value, final String idForn,
			final String prezzo, final String valoreAssoluto) {

		Scarto scarto = null;
		ProdFornito prodotto = null;
		ProdConcreto prodPadre = null;
		final HashMap<String, String> info = new HashMap<>();

		if (!catena.ottieniDallInventario(idProdFor).isPresent() && !idProdFor.isBlank()
				&& catena.ottieniDallInventario(idProdCon).isPresent()) {
			
			prodPadre = (ProdConcreto) catena.ottieniDallInventario(idProdCon).get();
			System.out.println("Il prodFornito non esiste e viene creato");

			prodotto = new ProdFornito(idProdFor, prodPadre);
			prodotto.setIDFornitore(idForn);
			prodotto.setPrezzo(Float.parseFloat(prezzo));
			prodotto.setValoreAssoluto(Float.valueOf(valoreAssoluto));
			prodotto.setValoreNetto(prodotto.getValoreNetto());
			prodotto.setPercentualeNetto(prodotto.getPercentualeNetto());
			prodotto.setPrezzoEffettivo(prodotto.getPrezzoEffettivo());

			if (!key.isBlank() && !value.isBlank()) {
				info.put(key, value);
				prodotto.aggiungiInfo(info);
			}
			if (!idScarto.isBlank() && !valoreScarto.isBlank()) {
				scarto = new Scarto(idScarto);
				scarto.setQuantita(Float.parseFloat(valoreScarto));
				this.scarti.add(scarto);
				prodotto.aggiungiScarti(this.scarti);
				catena.aggiungiScarti(this.scarti);
			}
			this.typology.add(prodotto);
			catena.aggiungiAllInventario(this.typology);
		}
		return prodotto;
	}

	@Override
	public Hotel addNewHotel(final Catena catena, final String idHotel, final String info) {

		Hotel hotel = null;

		if (!catena.ottieniUnAlbergo(idHotel).isPresent() && !idHotel.isBlank()) {
			System.out.println("Aggiungo un Hotel");
			hotel = new Hotel(idHotel);
			if (!info.isBlank()) {
				hotel.setInfo(info);
			}
			catena.aggiungiUnAlbergo(hotel);
		}
		return hotel;
	}

	@Override
	public Fornitore addNewFornitore(final Catena catena, final String idFornitore) {

		Fornitore fornitore = null;

		if (!idFornitore.isBlank() && !catena.ottieniUnFornitore(idFornitore).isPresent()) {
			System.out.println("Aggiuingo un nuovo fornitore");
			fornitore = new Fornitore(idFornitore);
			catena.aggiungiUnFornitore(fornitore);
		}
		return fornitore;
	}

	@Override
	public Dispensa addNewDispensa(final Catena catena, final String idHotel, final String idDispensa) {

		Dispensa dispensa = null;

		if (catena.ottieniUnAlbergo(idHotel).isPresent()) {
			if (!catena.ottieniUnAlbergo(idHotel).get().ottieniUnaDispensa(idDispensa).isPresent()
					&& !idDispensa.isBlank()) {
				System.out.println("Aggiungo la dispensa all'hotel");
				dispensa = new Dispensa(idDispensa, catena);
				catena.ottieniUnAlbergo(idHotel).get().aggiungiUnaDispensa(dispensa);
			}
		} else {
			System.out.println("Id hotel errato");
		}
		return dispensa;
	}
}
